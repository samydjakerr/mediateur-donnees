package org.example;

import java.util.ArrayList;

import org.apache.jena.query.ParameterizedSparqlString;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFactory;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.ResourceFactory;
public class DBpedia {
	
	ArrayList<String> producers = new ArrayList<>();
	ArrayList<String> directors = new ArrayList<>();
	ArrayList<String> actors = new ArrayList<>();
	//défnir les préfixes pour les différentes ontologies RDF utilisées dans la requête SPARQL.
	String prefixes = 
			"" +
			"PREFIX owl: <http://www.w3.org/2002/07/owl#>\n" +
            "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n" +
            "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" +
            "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
            "PREFIX foaf: <http://xmlns.com/foaf/0.1/>\n" +
            "PREFIX dc: <http://purl.org/dc/elements/1.1/>\n" +
            "PREFIX : <http://dbpedia.org/resource/>\n" +
            "PREFIX dbpedia2: <http://dbpedia.org/property/>\n" +
            "PREFIX dbpedia: <http://dbpedia.org/>\n" +
            "PREFIX dbo: <http://dbpedia.org/ontology/>\n" +
            "PREFIX dbr: <http://dbpedia.org/resource/>\n" +
            "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>\n";
	//récupérer le nom du réalisateur d'un film donné à partir de la base de données DBpedia.
	public ParameterizedSparqlString directorQuery(String movieName, String lang) {
		
		 ParameterizedSparqlString qs = new ParameterizedSparqlString
				 ( "" +
	                 prefixes +
	                "\n" +
	                "SELECT DISTINCT ?director\n" +
	                "WHERE {\n" +
	                "?film a <http://dbpedia.org/ontology/Film> ;\n" +
	                "foaf:name ?movieLiteral ;\n" +
	                "dbo:director ?directorObject .\n" +
	                "?directorObject foaf:name ?director .\n" +
	                "}"
	                );

	        Literal movieLiteral = ResourceFactory.createLangLiteral( movieName, lang );
	        qs.setParam( "movieLiteral", movieLiteral );
		return qs;
	}
	//récupérer le nom du producteur d'un film donné à partir de la base de données DBpedia.
	public ParameterizedSparqlString producerQuery(String movieName, String lang) {
		 //créer la requête SPARQL
		 ParameterizedSparqlString qs = new ParameterizedSparqlString
				 ( "" +
	                 prefixes +
	                "\n" +
	                "SELECT DISTINCT ?producer\n" +
	                "WHERE {\n" +
	                "?film a <http://dbpedia.org/ontology/Film> ;\n" +
	                "foaf:name ?movieLiteral ;\n" +
	                "dbo:producer ?producerObject .\n" +
	                "?producerObject foaf:name ?producer .\n" +
	                "}"
	                );
            //crée un littéral RDF
	        Literal movieLiteral = ResourceFactory.createLangLiteral( movieName, lang );
			//remplacer la variable movieLiteral dans la requête SPARQL
	        qs.setParam( "movieLiteral", movieLiteral );
		return qs;
	}
	////récupérer le nom de l'acteur d'un film donné à partir de la base de données DBpedia.
	public ParameterizedSparqlString actorQuery(String movieName, String lang) {
		
		 ParameterizedSparqlString qs = new ParameterizedSparqlString
				 ( "" +
	                 prefixes +
	                "" +
	                "SELECT DISTINCT ?actor\n" +
	                "WHERE {\n" +
	                "?film a <http://dbpedia.org/ontology/Film> ;\n" +
	                "foaf:name ?movieLiteral ;\n" +
	                "dbo:starring ?actorObject .\n" +
	                "?actorObject foaf:name ?actor .\n" +
	                "}"
	                );

	        Literal movieLiteral = ResourceFactory.createLangLiteral( movieName, lang );
	        qs.setParam( "movieLiteral", movieLiteral );
	        //System.out.println(qs);
		return qs;
	}
	//récupérer le nom d'un film ou un acteur donnée a joué à partir de la base de données DBpedia.
	public ParameterizedSparqlString movieQuery(String actor, String lang) {
		
		 ParameterizedSparqlString qs = new ParameterizedSparqlString
				 ( "" +
	                 prefixes +
	                "\n" +
	                "SELECT DISTINCT ?movie\n" +
	                "WHERE {\n" +
	                "?film a <http://dbpedia.org/ontology/Film> ;\n" +
	                "dbo:starring ?actorObject ;\n" +
	                "foaf:name ?movie .\n" +
	                "?actorObject foaf:name ?actor .\n" +
	                "}"
	                );

	        Literal actorLiteral = ResourceFactory.createLangLiteral( actor, lang );
	        qs.setParam( "actor", actorLiteral );
	        //System.out.println(qs);
		return qs;
	}
	//exécuter une requête SPARQL et retourner un résultat
	public ResultSet queryResults(ParameterizedSparqlString qs) {
		//Executer la requête SPARQL
		QueryExecution exec = QueryExecutionFactory.sparqlService( "http://dbpedia.org/sparql", qs.asQuery() );  
		//créer un objet ResultSet à partir des résultats de la requête
        ResultSet results = ResultSetFactory.copyResults( exec.execSelect() );
        return results;
	}


}
