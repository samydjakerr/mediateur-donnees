package org.example;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;


public class JDBC {
	private Connection conn = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    
    public void JDBC() {
    	
    }
	
	protected Movie readDataBase(String movie) throws Exception {
		Class.forName("com.mysql.jdbc.Driver");
        // Configurer la connexion avec la base de données.
        conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ied", "root", "");

        // Les instructions permettent d'émettre des requêtes SQL à la base de données.
        statement = conn.createStatement();
        // Un résultat de requête (result set) permet d'obtenir le résultat de la requête SQL.
        String request = "SELECT * FROM test";
        if (movie != null && !movie.equals("")) {
        	request += " WHERE Movie LIKE " + "\'" + movie + "\'";
        }
        resultSet = statement
                .executeQuery(request);
        
        Movie movieInformation = new Movie();
        //parcourir chaque ligne du résultat de la requête SQL stocké dans l'objet ResultSet
        //récupérer toutes les informations relatives à un film stocké dans la base de données.
        while (resultSet.next()) {

            if (resultSet.getString("Movie") != null) {
                movieInformation.setMovie(resultSet.getString("Movie"));
            }
            
            if (resultSet.getString("Genre") != null) {
                movieInformation.setGenre(resultSet.getString("Genre"));
            }
            if (resultSet.getString("Distributor") != null) {
                movieInformation.setDistributor(resultSet.getString("Distributor"));
            }
            if (resultSet.getString("ProductionBudget") != null) {
                movieInformation.setProductionBudget(resultSet.getString("ProductionBudget"));
            }
            if (resultSet.getString("DomesticGross") != null) {
                movieInformation.setDomesticGross(resultSet.getString("DomesticGross"));
            }
            if (resultSet.getString("WorldwideGross") != null) {
                movieInformation.setWorldwideGross(resultSet.getString("WorldwideGross"));
            }
            if (resultSet.getString("ReleaseDate") != null) {
                movieInformation.setReleaseDate(resultSet.getString("ReleaseDate"));
            }
        }
        // vérifier si les informations du film ont été correctement récupérées
        if (movieInformation.getMovie() != null && !movieInformation.getMovie().equals("")) {
        	return movieInformation;
        } else {
        	return null;
        }
        
	}

}
