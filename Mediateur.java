package org.example;
import org.apache.commons.lang3.text.WordUtils;

import java.util.Scanner;


import org.apache.jena.query.*;

public class Mediateur {

    public static void main(String[] args) throws Exception  {
		while(true) {
			System.out.println("Veuillez choisir parmis une des options suivantes : "
					+ "\n" + "----------------------------------------------------------------"
					+ "\n" + "Film- Chercher par nom de film " + "\n" + "Acteur- Chercher par nom d'acteur \n" +
					"Quitter- quitter le programme");
			Scanner scanner = new Scanner(System.in);
			String input = scanner.nextLine();
			if (input.equals("Film")) {
				System.out.print("Quel est le nom du film que vous voulez chercher ? \n");
				Scanner mscanner = new Scanner(System.in);
				String movie = scanner.nextLine();
				//mettre le movie en miniscule
				movie = movie.toLowerCase();
				//mettre la 1ere lettre de chaque mot en majuscule
				movie = WordUtils.capitalize(movie);
				try {
					JDBC jdbcConn = new JDBC();
					Movie movieInfo = jdbcConn.readDataBase(movie);
					if (movieInfo != null) {
						System.out.println("Film: " + movieInfo.getMovie());
						System.out.println("Date de sortie: " + movieInfo.getReleaseDate());
						System.out.println("Genre: " + movieInfo.getGenre());
						System.out.println("Distributeur: " + movieInfo.getDistributor());
						System.out.println("Budget de production: " + movieInfo.getproductionBudget());
						System.out.println("Revenus aux USA : " + movieInfo.getDomesticGross());
						System.out.println("Revenus international : " + movieInfo.getWorldwideGross());
					} else {
						System.out.println("Film non trouvé dans la base de données.");
					}
				} catch (Exception e) {
					System.out.println("Erreur: " + e.getMessage());
				}
				Movie mi = new Movie();
				XPath xq = new XPath();
				xq.xPathQueryResult(mi, movie); // Titre d'un film valide
				// Affiche le résumé du film
				System.out.println("Résumé : " + mi.getPlot()); 
				DBpedia DBpedia = new DBpedia();
				// --------------Recherche du réalisateur du film-----------
				ParameterizedSparqlString directorQuery = DBpedia.directorQuery(movie, "en");
				ResultSet directorResults = DBpedia.queryResults(directorQuery);
				System.out.println("Réalisateur : ");
				while (directorResults.hasNext()) {
						QuerySolution solution = directorResults.next();
						String directorName = solution.get("director").asLiteral().getString();
						System.out.println("- \t" + directorName);

				}
				// --------------Recherche du producteur du film-----------
				//construire la requête SPARQL et l'envoyer à l'API SPARQL 
				ParameterizedSparqlString producerQuery = DBpedia.producerQuery(movie, "en");
				//les résultats de la requête SPARQL
				ResultSet producerResults = DBpedia.queryResults(producerQuery);
				System.out.println("Producteur: ");
				while (producerResults.hasNext()) {
					QuerySolution solution = producerResults.next();
					String producerName = solution.get("producer").asLiteral().getString();
					System.out.println("-   \t" + producerName);
				}

				// // --------------Recherche de l'acteur du film-----------
				ParameterizedSparqlString actorQuery = DBpedia.actorQuery(movie, "en");
				ResultSet actorResults = DBpedia.queryResults(actorQuery);
				System.out.println("Acteur: ");
				while (actorResults.hasNext()) {
					QuerySolution solution = actorResults.next();
					String actorName = solution.get("actor").asLiteral().getString();
					System.out.println("-	\t" + actorName);
				}
			} else if (input.equals("Acteur")) {
				System.out.println("Donner le nom d'un acteur : ");
				DBpedia DBpedia = new DBpedia();
				Scanner ascanner = new Scanner(System.in);
				String actor = scanner.nextLine();
				actor= actor.toLowerCase();
				actor = WordUtils.capitalize(actor);
				System.out.println("Les films où "+ actor + " à joué sont : \n Films : ");
				ParameterizedSparqlString movieQuery = DBpedia.movieQuery(actor, "en");
				ResultSet movieResults = DBpedia.queryResults(movieQuery);
				while (movieResults.hasNext()) {
					QuerySolution solution = movieResults.next();
					String movieName1 = solution.get("movie").asLiteral().getString();
					try {
						JDBC jdbcConn = new JDBC();
						Movie movieInfo = jdbcConn.readDataBase(movieName1);
						System.out.println("-  " + movieName1);

						if (movieInfo != null) {
							System.out.println("Film: " + movieInfo.getMovie());
							System.out.println("Genre: " + movieInfo.getGenre());
							System.out.println("Distributeur: " + movieInfo.getDistributor());
							System.out.println("Release Date: " + movieInfo.getReleaseDate());
							System.out.println("----------------------");
						} else {
							System.out.println("Film non trouvé dans la base de données");
							System.out.println("----------------------");
						}
					} catch (Exception e) {
						System.out.println("Erreur: " + e.getMessage());
					}
				}
			} else if (input.equals("Quitter")) {
				System.out.println("Fin du programme \n");
				System.exit(0);
				break;
			} else {
				System.out.println("Mauvaise option");
			}


		}


	}
}
