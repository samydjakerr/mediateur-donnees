package org.example;

import javax.xml.xpath.*;
import javax.xml.namespace.QName;
import javax.xml.parsers.*;
import org.w3c.dom.*;


public class XPath {
	
	public void XPath() {
		
	}

    private static Object XPath(String uri, String requete, QName typeRetour){
		//Le dernier paramètre indique le type de résultat souhaité
		//XPathConstants.STRING: chaîne de caractères (String)
		//XPathConstants.NODESET: ensemble de noeuds DOM (NodeList)
		//XPathConstants.NODE: noeud DOM (Node) - le premier de la liste
		//XPathConstants.BOOLEAN: booléen (Boolean) - vrai si la liste n'est pas vide
		//XPathConstants.NUMBER: numérique (Double) - le contenu du noeud sélectionné transformé en Double

        try{
		//Transformation en document DOM du contenu XML
		DocumentBuilderFactory fabrique = DocumentBuilderFactory.newInstance();
		DocumentBuilder parseur = fabrique.newDocumentBuilder();
		Document document = parseur.parse(uri);

		//création de l'objet XPath 
        	XPathFactory xfabrique = XPathFactory.newInstance();
        	javax.xml.xpath.XPath xpath = xfabrique.newXPath();
        
        	//évaluation de l'expression XPath
        	XPathExpression exp = xpath.compile(requete);
        	return exp.evaluate(document, typeRetour);
        	
        } catch(Exception e){
        	System.out.println(e.getMessage());
        }
        return null;
    }
	
    /* @function : permet d'extraire le résumé d'un film à partir de l'API OMDB en utilisant une 
	requête XPATH et l'assigner au champ plot de l'objet Movie passé en paramètre.*/
    public void xPathQueryResult(Movie movie, String movieName) {
		//Remplaces tous les espaces 
    	movieName = movieName.replaceAll("\\s", "+");
    	String uri = "http://www.omdbapi.com/?apikey=f8d8402f&r=xml"+ "&t=" + movieName;
		//Requête XPath -> extraire la valeur de l'attribut plot de l'élément movie
    	Object output = XPath(uri, "/root/movie/@plot", XPathConstants.NODE);
    	if (output != null) {
			// convertir la variable output en un objet Node
    		Node node = (Node)output;
			//extraire le contenu textuel et vérifier la valeur du plot si n'est pas null
        	if (node.getTextContent() != null && !node.getTextContent().equals("")) {
				//enregitrer la valeur du plot
        		movie.setPlot(node.getTextContent());
        	}
    	}
    }
        	
}
