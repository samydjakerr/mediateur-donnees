package org.example;

import java.util.ArrayList;

public class Movie {
private String movieID;
private String movie;
private String releaseDate;
private String genre;
private String distributor;
private String productionBudget;
private String domesticGross;
private String worldwideGross;
private String plot;
private String rating;
private ArrayList<String> directors;
private ArrayList<String> producers;
private ArrayList<String> actors;

public void Movie() {
	
}
//l'identifiant du  film 
public String getMovieID() {
	return movieID;
}

public void setMovieID(String movieID) {
	this.movieID = movieID;
}
//le titre du film 
public String getMovie() {
	return movie;
}

public void setMovie(String movie) {
	this.movie = movie;
}
//Date de sortie
public String getReleaseDate() {
	return releaseDate;
}

public void setReleaseDate(String releaseDate) {
	this.releaseDate = releaseDate;
}
//genre du film 
public String getGenre() {
	return genre;
}

public void setGenre(String genre) {
	this.genre = genre;
}
//distributeur du film 
public String getDistributor() {
	return distributor;
}

public void setDistributor(String distributor) {
	this.distributor = distributor;
}
//Le budget de production 
public String getproductionBudget() {
	return productionBudget;
}

public void setProductionBudget(String productionBudget) {
	this.productionBudget = productionBudget;
}
//les revenus aux USA 
public String getDomesticGross() {
	return domesticGross;
}

public void setDomesticGross(String domesticGross) {
	this.domesticGross = domesticGross;
}
//les revenus a l'international 
public String getWorldwideGross() {
	return worldwideGross;
}

public void setWorldwideGross(String worldwideGross) {
	this.worldwideGross = worldwideGross;
}
//Le Résumé du film
public String getPlot() {
	return plot;
}

public void setPlot(String plot) {
	this.plot = plot;
}
//le Réalisateur du film
public ArrayList<String> getDirectors() {
	return directors;
}

public void setDirectors(ArrayList<String> directors) {
	this.directors = directors;
}
//le producteur du film 
public ArrayList<String> getProducers() {
	return producers;
}

public void setProducers(ArrayList<String> producers) {
	this.producers = producers;
}
//l'acteur du film 
public ArrayList<String> getActors() {
	return actors;
}

public void setActors(ArrayList<String> actors) {
	this.actors = actors;
}


}
