package code;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.FileWriter;
import java.io.IOException;

public class JSoup {

	public static void main(String[] args) throws IOException {

		String[] genres = {"Adventure", "Comedy", "Drama", "Action", "Thriller-or-Suspense", "Romantic-Comedy"};
	
        for (String genre : genres) {

            for (int year = 2000; year <= 2015; year++) {

                String url = "http://www.the-numbers.com/market/" + year + "/genre/" + genre;
                Document doc = Jsoup.connect(url).get();

                Elements movies = doc.select("table > tbody > tr:has(td)");

                FileWriter csvWriter = new FileWriter(genre + ".csv");
                csvWriter.append("Title,Release Date,Distributor\n");

                for (Element movie : movies) {
                    String title = movie.select("td:eq(0)").text();
                    String releaseDate = movie.select("td:eq(1)").text();
                    String distributor = movie.select("td:eq(2)").text();

                    csvWriter.append(title + "," + releaseDate + "," + distributor + "\n");
                }

                csvWriter.flush();
                csvWriter.close();
            }
        }
    }
}
